require 'gem2deb/rake/testtask'

skipped = %w[
  StackProfTest#test_walltime
  StackProfTest#test_gc
  StackProfTest#test_raw
].join("|")

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb']
  t.options = "--exclude='/#{skipped}/' --verbose"
end

task :cleanup do
  Dir.glob("**/lib/stackprof/stackprof.so").each do |f|
    FileUtils.rm_f(f)
  end
end

task :default => :cleanup
